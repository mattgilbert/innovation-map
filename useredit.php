<?php
session_start();
$servername = "localhost";
$username = "matt";
$password = "loose6Meat";
$dbname = "awesome";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
};
if(isset($_SESSION['userLoggedIn'])){
	if(isset($_POST['oldUsername']) && $_POST['oldUsername'] != "" && $_POST['newUsername'] != ""){
		$user = htmlspecialchars($_POST['oldUsername']);
		$newUser = htmlspecialchars($_POST['newUsername']);
		$sql = "SELECT password, salt FROM user WHERE username = '$user'";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$password = $row['password'];
				$salt = $row['salt'];
			};
			$form = htmlspecialchars($_POST['oldPass']);
			$hash = hash('sha512', $form);
			if($hash.$salt === $password.$salt){
				$newPass = htmlspecialchars($_POST['oldPass']);
				$newPass = hash('sha512', $newPass);
				$sql = "UPDATE user SET username='$newUser', password='$newPass' WHERE username='$user'";
				if($conn->query($sql) === TRUE){
					$_POST['edit'] = "success";
				}else{
					$_POST['edit'] = "failed";
				}
			}
		}
	}
?>

<!doctype html>

<html lang="en">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="spiral.css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript" src="edit.js"></script>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
<?php
if($_POST['edit'] === "success"){
	echo '<meta http-equiv="refresh" content="4;url=list.php">';
}
?>
<title>Edit User Login</title>

<body>

<form name="editForm" id="editForm" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
<?php
if($_POST['edit'] === "success"){
	echo 'Updated login information successfully.';
}elseif($_POST['edit'] === "failed"){
	echo 'There was an error with the update. Update failed.';
}
?>
	<label>Current Username: </label><input type="text" name="oldUsername">
	<br />
	<span class="oldUserError error"></span>
	<br />
	<label>New Username: </label><input type="text" name="newUsername">
	<br />
	<span class="newUserError error"></span>
	<br />
	<label>Current Password: </label><input type="password" name="oldPass">
	<br />
	<span class="oldPassError error"></span>
	<br />
	<label>New Password: </label><input type="password" name="newPass">
	<br />
	<span class="newPassError error"></span>
	<br />
	<br />
	<input name="submit" class="btn" type="submit" value="Submit">
	<a class="btn" href="list.php">Cancel</a>
</form>

</body>
</html>
<?php
}else{
	header("Location: list.php");
	die();
}
?>