<?php
session_start();
$servername = "localhost";
$username = "innomap";
$password = "YyUymm0TDF7saOir";
$dbname = "innomap";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
};

?>

<!doctype html>

<html lang="en">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="spiral.css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript" src="login.js"></script>
<script type="text/javascript" src="sha512.js"></script>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
<?php

if(isset($_POST['username']) && $_POST['username'] != ""){
	$user = htmlspecialchars($_POST['username']);
	$sql = "SELECT password, salt FROM user WHERE username = '$user'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$password = $row['password'];
			$salt = $row['salt'];
		};
		$form = htmlspecialchars($_POST['password']);
		$hash = hash('sha512', $form);
		if($hash.$salt === $password.$salt){
			$_SESSION['userLoggedIn'] = "awesome";
		}else{
			unset($_SESSION['userLoggedIn']);
			$_POST['error'] = "error";
		}
	}else{
		unset($_SESSION['userLoggedIn']);
		$_POST['empty'] = "empty";
	}
};

if(isset($_SESSION['userLoggedIn'])){
	echo '<title>Organisation List</title><body>';
	$sql = "SELECT name, id FROM moa";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    $d = 1;
	    while($row = $result->fetch_assoc()) {
	        echo "<div class='line'>".$d.": ".$row["name"]."</div><a class='btn editBtn' href='form.php?id=".$row["id"]."'>Edit</a><a class='btn deleteBtn' onclick='confirmDelete(".$row["id"].")'>Delete</a><br />";
	        $d++;
	    }
	} else {
	    echo "0 results";
	};
	echo '<a class="btn" href="form.php">Add New</a>&nbsp';
	echo '<a class="btn" href="useredit.php">Edit Admin</a>&nbsp';
    echo "<a class='btn' href='logout.php'>Logout</a>";
}else{
	echo '<title>List Login</title><body>';
	echo '<div id="loginContainer">';
		echo '<form name="loginForm" id="loginForm" action='.htmlspecialchars($_SERVER['PHP_SELF']).' method="post">';
			echo '<label>Username: </label>';
			echo '<input type="text" name="username">';
			echo '<br />';
			echo '<span class="userError error"></span>';
			echo '<br />';
			echo '<label>Password: </label>';
			echo '<input type="password" name="password">';
			echo '<br />';
			echo '<span class="passError error"></span>';
			echo '<br />';
			echo '<input class="btn" type="submit" name="submit" value="Submit">';
			echo '<br />';
			if(isset($_POST['error'])){
				echo "<span class='loginError'>Incorrect Username or Password.</span>";
			}elseif(isset($_POST["empty"])){
				echo "<span class='loginError'>Incorrect or empty Username.</span>";
			}
		echo '</form>';
	echo '</div>';
};

?>
</body>

</html>