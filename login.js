$(document).ready(function(){
	if($("form")[0]){
		$("form")[0].onsubmit = validateLogin;
	}
});

function validateLogin(e){
	if(validateUser() === false || validatePass() === false){
		e.preventDefault();
	}
}

function validateUser(){
	var user = document.forms["loginForm"]["username"].value;
	if(user === null || user === ""){
		$(".userError").html("Required");
		return false;
	}else if (user.match(/([^(\w+)*])+/g)) {
		$(".userError").html("Username can only contain letters");
		return false;
	}else{
		$(".userError").html("");
		return true;
	}
}

function validatePass(){
	var pass = document.forms["loginForm"]["password"].value;
	if(pass === null || pass === ""){
		$(".passError").html("Required");
		return false;
	}else if(pass.match(/([^(\w+)*])+/g)){
		$(".passError").html("Password can only contain letters and Numbers");
		return false;
	}else{
		$(".passError").html("");
		return true;
	} 
}

function confirmDelete(id){
	if(confirm("Are you sure you wish to delete this entry?")){
		deleteRow(id);
	}
}
function deleteRow(id){
	var formdata = new FormData();
	formdata.append("id",id);

	var theRequest = new XMLHttpRequest();
	theRequest.open("POST","delete.php");
	theRequest.onreadystatechange = handleDeleteResponse;
	theRequest.send(formdata);
};
function handleDeleteResponse(e){
	if(e.target.readyState === 4){
		// the response is stored in e.target.responseText
		alert(e.target.responseText)
		history.go(0);
	}
}