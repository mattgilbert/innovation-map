<?php
$servername = "localhost";
$username = "innomap";
$password = "YyUymm0TDF7saOir";
$dbname = "innomap";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$id = "";$name = "";$start = "";$end = "";$website = "";$description = "";$student = "";$migrant = "";$creative = "";$maori = "";$other = "";$startupbusiness = "";$socialenterprise = "";$project = "";$event = "";$abbrv = "";$serviceList = array();
$jsonResponse = array("organisations" => array());
$sql = "SELECT * FROM moa";
$statement = $conn->prepare($sql);
$statement->execute();
$statement->store_result();
$statement->bind_result($id,$name,$start,$end,$website,$description,$student,$migrant,$creative,$maori,$other,$startupbusiness,$socialenterprise,$project,$event,$abbrv);
if($statement->num_rows === 0){
	echo "no data found";
}
while($statement->fetch()){
	$jsonRow = array("orgName"=>$name, "Start"=>$start, "End"=>$end, "Website"=>$website, "Description"=>$description, "Student" =>$student, "Migrant" =>$migrant, "Creative" =>$creative, "Maori" =>$maori, "Other" =>$other, "Start-Up Business" =>$startupbusiness, "Social Enterprise" =>$socialenterprise, "Project" =>$project, "Event" =>$event, "ID" => $id, "Abbrv" => $abbrv);
	$jsonResponse["organisations"][] = $jsonRow;
};
$statement->close();
$sql2 = "SELECT * FROM Services";
$statement = $conn->prepare($sql2);
$statement->execute();
$statement->store_result();
$statement->bind_result($id, $service, $moaID);
if($statement->num_rows === 0){
	die("no data found");
}
while($statement->fetch()){
	if(!array_key_exists($moaID, $serviceList)){
		$serviceList[$moaID] = array();
	}
	$serviceList[$moaID][] = $service;
}
?>
<!doctype html>

<html lang="en-GB">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9">
	
	<script>
	var organisations =  <?php echo json_encode($jsonResponse); ?>;
	var services =  <?php echo json_encode($serviceList); ?>;
	</script>

	<script type="text/javascript" src="jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="modernizr.js"></script>
	<script type="text/javascript" src="scroll.js"></script>
	<script type="text/javascript" src="spiral.js"></script>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="spiral.css">
	<link rel="stylesheet" type="text/css" href="print.css" media="print">
	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
	<title>Innovation Ecosystem Map</title>
</head>

<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script type="text/javascript">
		window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));
	</script>

	<div id="right">
		<a href="index.php"><img id="logo" src="assets/Logo_PH.png" alt="Logo" /></a>
		<div id="about"><h4>About:</h4>The Canterbury Innovation Ecosystem Map was created as a collaboration between Ministry of Awesome and the Canterbury Development Corporation to help entrepreneurs and innovators easily find the resources available to support them. If you are looking for further support, get in touch with Ministry of Awesome!</div>
		<input type="button" id="reset" value="Reset Map" style="z-index:10;">
		<div id="share">
			<div class="fb-share-button" data-href="http://www.innovationmap.co.nz/" data-layout="button"></div>
			<a class="twitter-share-button"  href="http://www.innovationmap.co.nz/" data-count="none">Tweet</a>
		</div>
	</div>
	<div class="top buttonContainer">
		<div class="peopleContainer">
			<h4>Who are you?</h4><br />
			<div class="buttonWrapPeople"><input type="image" id="Student" class="people button" src="assets/Student_PH.png" alt="Student"><div>Student</div></div>
			<div class="buttonWrapPeople"><input type="image" id="Migrant" class="people button" src="assets/Migrant_PH.png" alt="Migrant"><div>Migrant</div></div>
			<div class="buttonWrapPeople"><input type="image" id="Creative" class="people button" src="assets/Creative_PH.png" alt="Creative"><div>Creative</div></div>
			<div class="buttonWrapPeople"><input type="image" id="Maori" class="people button" src="assets/Koru_PH.png" alt="Maori"><div>Maori</div></div>
			<div class="buttonWrapPeople"><input type="image" id="Other" class="people button" src="assets/Other_PH.png" alt="Other"><div>Other</div></div>
		</div>
		<div class="activitiesContainer">
			<h4>What are you doing?</h4><br />
			<div class="buttonWrapActivities"><input type="image" id="startup" class="activities button" src="assets/StartupBusiness_PH.png" alt="Start-Up Business"><div>Startup</div></div>
			<div class="buttonWrapActivities"><input type="image" id="social" class="activities button" src="assets/Social Enterprise_PH.png" alt="Social Enterprise"><div>Social Enterprise</div></div>
			<div class="buttonWrapActivities"><input type="image" id="project" class="activities button" src="assets/Project_PH.png" alt="Project"><div>Project</div></div>
			<div class="buttonWrapActivities"><input type="image" id="event" class="activities button" src="assets/Event_PH.png" alt="Event"><div>Event</div></div>
		</div>
	</div>
	<!-- Spiral Canvases -->
	<div class="canvascontainer">
		<img id="backcanvas" src="assets/Spiral Circles.png" style="z-index:1;">
		<img class="stagebar" src="assets/center.png" style="z-index:9;">
		<img class="stagebar ideaStage" src="assets/ideation.png" style="z-index:8;">
		<img class="stagebar planStage" src="assets/plan.png" style="z-index:7;">
		<img class="stagebar startStage" src="assets/start.png" style="z-index:6;">
		<img class="stagebar growStage" src="assets/grow.png" style="z-index:5;">
		<canvas id="canvas" width="600" height="600" style="z-index:2;">
			<img id="canvas_replace" src="assets/canvas.png" />
		</canvas>
	</div>
	
	<!-- Footer area for social media links -->
	
	<div id="footer">
		<div class="powered">
			<img class="powered poweredText" src="assets/Powered by_text.png">
			<a href="http://www.ministryofawesome.com/"><img class="powered" src="assets/MOALogo.png"></a>
			<a href="http://www.cdc.org.nz"><img class="powered" src="assets/CDCLogo.png"></a>
		</div>
	</div>

	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->

	<!-- Listing Section -->
	<div class="sidebar group top">
		<ul class="list1 top">
			<li class="ideation inline">Ideation</li>
			<li class="plan inline">Plan</li>
			<li class="start inline">Start</li>
			<li class="grow inline">Grow</li>
		</ul>
		<div id = "stages"></div>
		<p id = "disclaimer" class="hidden">No organisations meet your criteria, please select additional people and/or activities.</p>
	</div>
	<script src="jquery.gray.min.js"></script>
</body>

</html>