// canvas draw section begins	

function canvas(){

	// Canvas for Text
	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");
	ctx.beginPath();
	ctx.font="8pt Verdana";
	ctx.fillStyle="blue";
	ctx.fill;
	ctx.translate(300,300);
};
// canvas draw section ends




var firstTime = true;

var FullList = organisations;

var ServiceList = services;


// Model Code

var currentList = FullList;

function UpdateModel(people, activities){

   currentList=CreateList(people,activities);

 }

function CreateList(people, activities){
//  Filter FullList to include only the items that have the at least one of the people and activities and put results in list
	var list1 = [];
	var list2 = [];
	var buttonName = "";
	var test = "";

	var list1Len = FullList.organisations.length;
	
	for(var person in people){
		//buttonName = people[person].innerHTML;
		buttonName = people[person].getAttribute("alt");
		for(var org = 0; org < list1Len; org++){
			test = FullList.organisations[org][buttonName]
			if(test === 1){
				list1.push(FullList.organisations[org]);
			}
		};
	}
	var list2Len = list1.length;
	for(var activity in activities){
		act = activities[activity].getAttribute("alt");
		for(var org in list1){
			thing = list1[org][act];
			if(thing === 1 && list2.indexOf(list1[org]) === -1){
				list2.push(list1[org])
			}
		}
	}
	return list2;
}






// Controller Code

// event listeners

$(document).ready(function(){
	if(Modernizr.canvas){
		canvas();
		convert(FullList);
		fly();
		$(".people").on("click",onPress);
		$(".activities").on("click",onPress);
		$("#reset").on("click",run);
		$("#canvas").on("click", canvasClick).on("mousemove", canvasMouseOver);
	}else{
		$("#canvas_replace").css("display","")
		convert(FullList);
		flyIE();
		$(".people").on("click",onPress);
		$(".activities").on("click",onPress);
		$("#reset").on("click",run);
	}
});

function run(){
	var clear = document.getElementById("canvas");
	var ctx = clear.getContext('2d');
	ctx.clearRect(-300, -300, clear.width, clear.height);

	currentList = organisations;

	for(var count = 0; count < Business.all.length; count++){
		Business.all[count].close = "";
		Business.all[count].tag = "yes";
	}

	$(".toggle").remove();
	$(".drop").remove();
	$(".container").remove();

	$(".people").removeClass("grayscale");
	$(".people").next().removeClass("grey");
	$(".activities").removeClass("grayscale");
	$(".activities").next().removeClass("grey");

	firstTime = true;

	rotateAll();

	$("#disclaimer").addClass("hidden");

	listInit();
}

function changeSelections(button){
// add the class grey to all buttons except whichButton (two loops, one for people and one for activities)
	var activity = $(".activities").length;
	var person = $(".people").length;

   	if (firstTime){
   		for(i=0;i<person;i++){
   			$(".people").addClass("grayscale");
   			$(".people").next().addClass("grey");
   			$(button).removeClass("grayscale");
   			$(button).next().removeClass("grey");
   		}

   		for(i=0;i<activity;i++){
   			$(".activities").addClass("grayscale");
   			$(".activities").next().addClass("grey");
   			$(button).removeClass("grayscale");
   			$(button).next().removeClass("grey");
   		}

		firstTime = false;

   	};

	var people = $(".people").not(".grayscale").toArray();

	var activities = $(".activities").not(".grayscale").toArray();

   	UpdateModel(people,activities);

}

 function onPress(){
// the code called by each button

   toggleMe(this);

   changeSelections(this);

   updateWheel();

   updateOnScreenList();
}







// View Code

function toggleMe(button){
	// changes button colour on click
   	if($(button).hasClass("grayscale")){
   		$(button).removeClass("grayscale");
   		$(button).next().removeClass("grey");
   	}else{
   		$(button).addClass("grayscale");
   		$(button).next().addClass("grey");
	};
};

function updateWheel(){
// start your animation to update the wheel
	for(var i in Business.all){
		Business.all[i].tag = "no";
		for(var t in currentList){
			if(currentList[t].orgName === Business.all[i].name)
				Business.all[i].tag = "yes";
		};
	};
	rotateAll();
};

function updateOnScreenList(){
// update the list
	$(".toggle").remove();
	$(".drop").remove();
	$(".container").remove();
	var start, end;
	for(var i in currentList){

		var title = currentList[i].orgName.toLowerCase();
		title = title.replace(/\(/g, "");
		title = title.replace(/\)/g, "");
		title = title.replace(/\s/g, "");

		if($("h4."+title).length === 0){


			if(currentList[i].Start === "Ideation" && currentList[i].End === "Ideation"){
				end = "oneWide";
			}else if(currentList[i].Start === "Ideation" && currentList[i].End === "Plan"){
				end = "twoWide";
			}else if(currentList[i].Start === "Ideation" && currentList[i].End === "Start"){
				end = "threeWide";
			}else if(currentList[i].Start === "Ideation" && currentList[i].End === "Grow"){
				end = "fourWide";
			}else if(currentList[i].Start === "Plan" && currentList[i].End === "Plan"){
				end = "oneWide";
			}else if(currentList[i].Start === "Plan" && currentList[i].End === "Start"){
				end = "twoWide";
			}else if(currentList[i].Start === "Plan" && currentList[i].End === "Grow"){
				end = "threeWide";
			}else if(currentList[i].Start === "Start" && currentList[i].End === "Start"){
				end = "oneWide";
			}else if(currentList[i].Start === "Start" && currentList[i].End === "Grow"){
				end = "twoWide";
			}else if(currentList[i].Start === "Grow" && currentList[i].End === "Grow"){
				end = "oneWide";
			}
			// set how many columns the business crosses


			if(currentList[i].Start === "Ideation"){
				start = "colOne";
			}else if(currentList[i].Start === "Plan"){
				start = "colTwo";
			}else if(currentList[i].Start === "Start"){
				start = "colThree";
			}else if(currentList[i].Start === "Grow"){
				start = "colFour";
			}
			// set which column the business starts at


			var person = [];
			if(currentList[i].Student === "1"){
				person += "student ";
			}
			if(currentList[i].Migrant === "1"){
				person += "migrant ";
			}
			if(currentList[i].Creative === "1"){
				person += "creative ";
			}
			if(currentList[i].Maori === "1"){
				person += "maori ";
			}
			if(currentList[i].Other === "1"){
				person += "other";
			}
			// setting class names for people


			var activity = [];
			if(currentList[i].StartUpBusiness === "1"){
				activity += "startupbusiness ";
			}
			if(currentList[i].SocialEnterprise === "1"){
				activity += "socialenterprise ";
			}
			if(currentList[i].Project === "1"){
				activity += "project ";
			}
			if(currentList[i].Event === "1"){
				activity += "event";
			}
			// setting class names for activities


			var description = currentList[i].Description;
			var website = currentList[i].Website;

			$("#stages").append("<div class = 'container group "+person+" "+activity+"'></div>");

			var h4 = $("<h4 class = 'toggle "+title+" "+start+" "+end+"' onclick='toggle(this)'>"+currentList[i].orgName+" <span class='arrow'>"+String.fromCharCode("9660")+"</span></h4>");
			var div = $("<div class='drop hidden "+title+"'></div>");
			var head = $("<h3 class='heading'>"+currentList[i].orgName+"</h3>");
			var p = $("<p class='description'>"+description+"</p>");
			var link = $('<a href='+website+'>'+website+'</a>');
			var list = $("<div class='right'><span class='core'>Core Services:</span><ul class='ul'></ul></div>");

			$(".container").css("margin-top", "0")

			$($(".container")[i]).append(h4).append(div);

			$($(".drop")[i]).append(head).append(link).append("<br /><br />").append(p).append(list);

			var ident = currentList[i].ID;
			for(var service in ServiceList[ident]){
				var thing = ServiceList[ident][service];
				services = $("<li>&#8226 "+thing+"</li>");
				$($(".ul")[i]).append(services);
			}
			// listing services
		};
	};
	if($(".container").length != 0){
		$("#disclaimer").addClass("hidden");
	}else{
		$("#disclaimer").removeClass("hidden");
	};
	$(".container").last().css("border-bottom","2px solid black");
};

function listInit(){
	var start, end;
	var len = currentList.organisations.length;
	for(var i = 0; i < len; i++){
		var title = currentList.organisations[i].orgName.toLowerCase();
		title = title.replace(/\(/g, "");
		title = title.replace(/\)/g, "");
		title = title.replace(/\s/g, "");
		if($("h4."+title).length === 0){


			if(currentList.organisations[i].Start === "Ideation" && currentList.organisations[i].End === "Ideation"){
				end = "oneWide";
			}else if(currentList.organisations[i].Start === "Ideation" && currentList.organisations[i].End === "Plan"){
				end = "twoWide";
			}else if(currentList.organisations[i].Start === "Ideation" && currentList.organisations[i].End === "Start"){
				end = "threeWide";
			}else if(currentList.organisations[i].Start === "Ideation" && currentList.organisations[i].End === "Grow"){
				end = "fourWide";
			}else if(currentList.organisations[i].Start === "Plan" && currentList.organisations[i].End === "Plan"){
				end = "oneWide";
			}else if(currentList.organisations[i].Start === "Plan" && currentList.organisations[i].End === "Start"){
				end = "twoWide";
			}else if(currentList.organisations[i].Start === "Plan" && currentList.organisations[i].End === "Grow"){
				end = "threeWide";
			}else if(currentList.organisations[i].Start === "Start" && currentList.organisations[i].End === "Start"){
				end = "oneWide";
			}else if(currentList.organisations[i].Start === "Start" && currentList.organisations[i].End === "Grow"){
				end = "twoWide";
			}else if(currentList.organisations[i].Start === "Grow" && currentList.organisations[i].End === "Grow"){
				end = "oneWide";
			}
			// set how many columns the business crosses


			if(currentList.organisations[i].Start === "Ideation"){
				start = "colOne";
			}else if(currentList.organisations[i].Start === "Plan"){
				start = "colTwo";
			}else if(currentList.organisations[i].Start === "Start"){
				start = "colThree";
			}else if(currentList.organisations[i].Start === "Grow"){
				start = "colFour";
			}
			// set which column the business starts at

			var person = [];
			var activity = [];
			var services = [];

			if(currentList.organisations[i].Student === "1"){
				person += "student ";
			}
			if(currentList.organisations[i].Migrant === "1"){
				person += "migrant ";
			}
			if(currentList.organisations[i].Creative === "1"){
				person += "creative ";
			}
			if(currentList.organisations[i].Maori === "1"){
				person += "maori ";
			}
			if(currentList.organisations[i].Other === "1"){
				person += "other";
			}
			// setting class names for people

			if(currentList.organisations[i].StartUpBusiness === "1"){
				activity += "startupbusiness";
			}
			if(currentList.organisations[i].SocialEnterprise === "1"){
				activity += "socialenterprise";
			}
			if(currentList.organisations[i].Project === "1"){
				activity += "project";
			}
			if(currentList.organisations[i].Event === "1"){
				activity += "event";
			}
			// setting class names for activities


			var description = currentList.organisations[i].Description;
			var website = currentList.organisations[i].Website;
			var h4 = $("<h4 class = 'toggle "+title+" "+start+" "+end+"' onclick='toggle(this)'>"+currentList.organisations[i].orgName+" <span class='arrow'>"+String.fromCharCode("9660")+"</span></h4>");
			var div = $("<div class='drop hidden "+title+"'></div>");
			var head = $("<h3 class='heading'>"+currentList.organisations[i].orgName+"</h3>");
			var p = $("<p class='description'>"+description+"</p>");
			var link = $('<a href='+website+'>'+website+'</a>');
			var list = $("<div class='right'><span class='core'>Core Services:</span><ul class='ul'></ul></div>");

			$("#stages").append("<div class = 'container group gradient "+person+" "+activity+"'></div>")

			$($(".container")[i]).append(h4).append(div);
			var top = (50*(i+1));
			$($(".container")[i]).animate({"top":top+"px"},400);

			$($(".drop")[i]).append(head).append(link).append("<br /><br />").append(p).append(list);

			var ident = currentList.organisations[i].ID;
			for(var service in ServiceList[ident]){
				var thing = ServiceList[ident][service];
				services = $("<li>&#8226 "+thing+"</li>");
				$($(".ul")[i]).append(services);
			};
			// listing services
		};
	};
	$(".container").last().css("border-bottom","2px solid black");
};

function toggle(element){
// Show/Hide Toggle functionality for list items
	$(element).next().slideToggle(500);
};

function canvasClick(event){
	canvas_x = event.pageX;
	canvas_y = event.pageY;
	var top = $(window).scrollTop();
	canvas_y = canvas_y - top;
	var rad = Math.atan2((320 - canvas_y),(733 - canvas_x))
	rad += Math.PI;
	for(var count in Business.all){
		var stop = (Business.all[count].stop);
		if(rad >= (stop - 0.07) && rad <= (stop + 0.07)){
			var name = Business.all[count].name;
			len = FullList.organisations.length;
			for(var i = 0; i < len; i++){
				var name2 = FullList.organisations[i].orgName;
				if(name === name2){
					ele = name.toLowerCase();
					ele = ele.replace(/\s+/g, '');
					ele = ele.replace(/\(+/g, '');
					ele = ele.replace(/\)+/g, '');
					ele = "."+ele;
					$(ele).next().slideToggle(500, function(){$("div"+ele).scrollintoview({duration: 600})});
					return true;
				};
			};
		};
	};
};

function canvasMouseOver(event){
	canvas_x = event.pageX;
	canvas_y = event.pageY;
	var top = $(window).scrollTop();
	canvas_y = canvas_y - top;
	var rad = Math.atan2((320 - canvas_y),(733 - canvas_x))
	rad += Math.PI;
	var foundYellow = false;
	for(var count in Business.all){
		var stop = (Business.all[count].stop);
		if(rad >= (stop - 0.07) && rad <= (stop + 0.07)){
			var name = Business.all[count].name;
			len = FullList.organisations.length;
			for(var i = 0; i < len; i++){
				var name2 = FullList.organisations[i].orgName;
				if(name === name2){
					$("#canvas").css("cursor", "pointer");
					foundYellow = true;
				};
			};
		};
	};
	if(!foundYellow){
		$("#canvas").css("cursor", "");
	};
};










// Wheel Code

function flyIE(){
	$('#backcanvas').animate({"top":"700px"},800,function(){
		$(".ideaStage").animate({"left":"345px"},400,function(){
			$(".planStage").animate({"left":"414px"},400,function(){
				$(".startStage").animate({"left":"477px"},400,function(){
					$(".growStage").animate({"left":"538px"},400,function(){
						listInit();
					});
				});
			});
		});
	});
}

function fly(){
	$('#backcanvas').animate({"top":"700px"},800,function(){
		$(".ideaStage").animate({"left":"345px"},400,function(){
			$(".planStage").animate({"left":"414px"},400,function(){
				$(".startStage").animate({"left":"477px"},400,function(){
					$(".growStage").animate({"left":"538px"},400,function(){
						animateSpiral();
					});
				});
			});
		});
	});
}

function convert(list){
	var start, end;
	for(var org in list.organisations){
		var name = list.organisations[org].orgName;
		if(list.organisations[org].Start === "Ideation"){
			start = 0;
		}else if(list.organisations[org].Start === "Plan"){
			start = 0.55;
		}else if(list.organisations[org].Start === "Start"){
			start = 1;
		}else if(list.organisations[org].Start === "Grow"){
			start = 1.5;
		}
		if(list.organisations[org].End === "Ideation"){
			end = 0.55;
		}else if(list.organisations[org].End === "Plan"){
			end = 1;
		}else if(list.organisations[org].End === "Start"){
			end = 1.55;
		}else if(list.organisations[org].End === "Grow"){
			end = 2;
		}
		var tag = "yes";
		var abbrv = list.organisations[org].Abbrv;
		new Business(name, start, end, tag, abbrv);
	}
	// converting info from the org list into the business array for use in the draw functions
}

function Business(name, start, end, tag, abbrv){
// sets array for the rotation display
	this.name = name;
	this.start = start;
	this.end = end;
	this.tag = tag;
	this.abbrv = abbrv;
	if(!Business.all){
		Business.all = []
	};
	Business.all.push(this);
};

function getAngle(){

// returns rotation information about how many items to calulate for.

	var arrayLen = Business.all.length;
	var arrayLen = arrayLen+1;
	return 100/arrayLen;
};

function draw(business){

// displays color and text at the rotations for one item of array

	// finding size and shape and location fron the center of each item
	var start = business.start*129+47;
	var end = business.end*125+48;

	var tag = business.tag;

	var len = end-start;
	var len2 = len -20;

	var mid = start+10;

	if(tag === "yes"){
		var color = "#FFF200";
	}else{
		var color = "grey";
	};

	// setting color here

	var rotation = getAngle();
	var index = Business.all.indexOf(business);
	var index = index+1;

	var degrees = (rotation*index)/100;
	degrees = degrees*360;

	rotation = degrees*Math.PI/180;
	// finding the rotation from 0

	var ctx = document.getElementById("canvas").getContext('2d');
	ctx.moveTo(300,300);
	ctx.rotate(rotation);
	// console.log(business.name)
	// console.log(rotation)
	ctx.fillStyle="white";
	ctx.fillRect(65,0,15,2);
	ctx.fillRect(95,0,15,2);
	ctx.fillRect(125,0,15,2);
	ctx.fillRect(155,0,15,2);
	ctx.fillRect(185,0,15,2);
	ctx.fillRect(215,0,15,2);
	ctx.fillRect(245,0,15,2);
	ctx.fillRect(275,0,15,2);
	ctx.fillStyle=color;
	ctx.fillRect(start,-8,len,20);
	ctx.fillStyle = "black";
	ctx.font = "12px Oxygen, Arial";
	var wide = ctx.measureText(business.name);
	if(wide.width > len2){
		ctx.fillText(business.abbrv,mid,6,len2);
	}else{
		ctx.fillText(business.name,mid,6,len2);
	}
	ctx.rotate(-rotation); 
	// draws text items and backing color
};

function rotateAll(){

// runs the rotation over entire Business array, also clears canvas before drawing so not to overlay

	var clear = document.getElementById("canvas");
	var ctx = clear.getContext('2d');
	ctx.clearRect(-300, -300, clear.width, clear.height);

	for(var count in Business.all){
		draw(Business.all[count]);
	};
};


// spiral animation
var can, ctx, addAngle, step, turn, steps = 150, delay = 20;

function animateSpiral(){

// animation handler

	can = document.getElementById("canvas");
	ctx = can.getContext("2d");
    addAngle = Math.PI * 2 / steps;// decides how large each step needs to be based on how many steps
	step = 0;
	turn = 0;
	assignStop();
	RotateTextSphere();
};

function RotateTextSphere() {

// animation steps

	step++;
	ctx.clearRect(-300, -300, can.width, can.height);
	ctx.save();
	turn = Math.round((addAngle*step)*10)/10;
	for(var count=0; count<Business.all.length;count++){
		var stop = Math.round((Business.all[count].stop)*10)/10;
		if(turn >= stop){
			Business.all[count].close = "done";
			draw(Business.all[count]);// calls function that draws items at specific rotations
		}
	}
	ctx.rotate(addAngle * step);
	spiral();// calls function that draws the items at any rotation
	ctx.restore();
	if (step < steps){
	 	requestAnimationFrame(RotateTextSphere);
	} else {
		listInit();
	}

};

function spiral(){

// initial spiral effect on page load.

	var color = "#FFF200"

	// finding size and shape and location fron the center of each item
	for(var count in Business.all){
		if(Business.all[count].close != "done"){
			var start = Business.all[count].start*129+47;
			var end = Business.all[count].end*125+48;

			var len = end-start;
			var len2 = len -20;

			var mid = start+10;

			var ctx = document.getElementById("canvas").getContext('2d');
			ctx.fillStyle="white";
			ctx.fillRect(65,0,15,2);
			ctx.fillRect(95,0,15,2);
			ctx.fillRect(125,0,15,2);
			ctx.fillRect(155,0,15,2);
			ctx.fillRect(185,0,15,2);
			ctx.fillRect(215,0,15,2);
			ctx.fillRect(245,0,15,2);
			ctx.fillRect(275,0,15,2);
			ctx.fillStyle=color;
			ctx.fillRect(start,-8,len,20);
			ctx.fillStyle = "black";
			ctx.font = "12px Oxygen, Arial";
			var wide = ctx.measureText(Business.all[count].name);
			if(wide.width > len2){
				ctx.fillText(Business.all[count].abbrv,mid,6,len2);
			}else{
				ctx.fillText(Business.all[count].name,mid,6,len2);
			}
			// draws text items and backing color
		}
	};
};

function assignStop(){

// assigns stop rotation angle to business array

	var rotation = getAngle();

	for(var count in Business.all){
		
		var index = (parseInt(count)+1)
		degrees = (rotation*index)/100;
		degrees = degrees*360;
		end = degrees*Math.PI/180;
		Business.all[count].stop = end;
	};
};




// request animation frame code for compatability accross borswers
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
