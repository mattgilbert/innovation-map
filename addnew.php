<?php
$servername = "localhost";
$username = "innomap";
$password = "YyUymm0TDF7saOir";
$dbname = "innomap";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
if(isset($_POST["name"])){

	$name = $_POST['name'];
	$start = $_POST['start'];
	$end = $_POST['end'];
	$website = rtrim($_POST['website'],"/");
	$desc = $_POST['description'];
	$student = isset($_POST['student']);
	$migrant = isset($_POST['migrant']);
	$creative = isset($_POST['creative']);
	$maori = isset($_POST['maori']);
	$other = isset($_POST['other']);
	$sub = isset($_POST['startUpBusiness']);
	$se = isset($_POST['socialEnterprise']);
	$project = isset($_POST['project']);
	$event = isset($_POST['event']);
	$serviceList = $_POST['service'];
	$abbrv = $_POST['abbrv'];

	if (empty($_POST["name"])) {
	 $nameError = "Name is required";
	} else {
	 $name = test_input($_POST["name"]);
	}
	 
	if (empty($_POST["website"])) {
	 $website = "Website is required";
	} else {
	 $website = test_input($_POST["website"]);
	}

	if (empty($_POST["description"])) {
	 $descError = "Description is required";
	} else {
	 $desc = test_input($_POST["description"]);
	}

	$sql = "INSERT INTO moa (name, start, end, website, description, student, migrant, creative, maori, other, startupbusiness, socialenterprise, project, event, abbrv) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	$statement = $conn->prepare($sql);
	$statement->bind_param("sssssiiiiiiiiis",$name,$start,$end,$website,$desc,$student,$migrant,$creative,$maori,$other,$sub,$se,$project,$event,$abbrv);
	$statement->execute();
	$statement->close();

	foreach($serviceList as $key=>$service){
		$sql = "SELECT id FROM moa WHERE name = ?";
		$statement = $conn->prepare($sql);
		$statement->bind_param("s",$name);
		$statement->execute();
		$statement->bind_result($id);
		if(!$statement->fetch()){
			echo "no data found";
		}
		$statement->close();
		$sql = "INSERT INTO Services (service, moaID) value (?, ?)";
		$statement = $conn->prepare($sql);
		$statement->bind_param("si", $service, $id);
		$statement->execute();
	}

	header('location: http://www.innovationmap.co.nz/list.php');
}

?>