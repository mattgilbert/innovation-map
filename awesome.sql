-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 16, 2015 at 02:08 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `awesome`
--

-- --------------------------------------------------------

--
-- Table structure for table `moa`
--

CREATE TABLE IF NOT EXISTS `moa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `student` tinyint(1) NOT NULL,
  `migrant` tinyint(1) NOT NULL,
  `creative` tinyint(1) NOT NULL,
  `maori` tinyint(1) NOT NULL,
  `other` tinyint(1) NOT NULL,
  `startupbusiness` tinyint(1) NOT NULL,
  `socialenterprise` tinyint(1) NOT NULL,
  `project` tinyint(1) NOT NULL,
  `event` tinyint(1) NOT NULL,
  `Abbrv` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `moa`
--

INSERT INTO `moa` (`id`, `name`, `start`, `end`, `website`, `description`, `student`, `migrant`, `creative`, `maori`, `other`, `startupbusiness`, `socialenterprise`, `project`, `event`, `Abbrv`) VALUES
(21, 'Akina Foundation', 'Ideation', 'Grow', 'http://www.akina.org.nz', 'Our mission is to grow the emerging New Zealand social enterprise sector by:\r\nActivating talent, raising awareness and building capability for social enterprise. Supporting high-potential social enterprises to deliver scalable solutions to pressing social and environmental challenges. Facilitating new market and investment opportunities for social enterprise.', 1, 1, 1, 1, 1, 0, 1, 0, 0, 'Akina'),
(22, 'Business Mentors (CDC)', 'Ideation', 'Grow', 'http://www.businessmentors.org.nz/Contact-Us/canterbury.aspx', 'Business Mentors New Zealand provides one on one, face to face business mentoring to SME businesses owner operators by volunteer business mentors.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'BM (CDC)'),
(23, 'Social Enterprise Fund (TCCT)', 'Grow', 'Grow', 'http://www.commtrust.org.nz/social-enterprise-fund', 'The $2.5Million dollar Social Enterprise Fund will enable projects that enhance the reputation of Canterbury as an idyllic place to live and visit, and provide the following community returns: Help support employment opportunities and outcomes, encourage innovation and self sufficiency and encourage community participation', 0, 0, 0, 0, 1, 0, 1, 0, 0, 'TCCT'),
(24, 'Canterbury Development Corporation (CDC)', 'Plan', 'Grow', 'http://www.cdc.org.nz', 'Canterbury Development Corporation (CDC) is the economic development agency for Christchurch City Council. CDC aims to drive regional economic growth for the benefit of the community. Plan and forecast economic and workforce dynamics which accelerate the key productivity drivers in the region. Identify sectors and businesses with high growth potential and support their development. Be the national leader in the commercialisation of innovation.  Complement and support national and local government aims and initiatives.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'CDC'),
(25, 'EntrÃ©', 'Ideation', 'Start', 'http://www.entre.canterbury.ac.nz', 'A not-for-profit University of Canterbury company, run by students for all tertiary students in the Canterbury region. EntrÃ© exists to foster an entrepreneurial spirit, to encourage innovation and creativity and to educate students on the basic elements of business development.Â  Through competitions, educational workshops (e.g Fast Track) and networking events, EntrÃ© seeks to provide students from all disciplines with real business-world experience.Â  EntrÃ© is committed to providing students with hands-on opportunities and industry exposure - things that are not provided in the lecture theatre.', 1, 0, 1, 1, 1, 1, 1, 0, 0, 'EntrÃ©'),
(26, 'UC Innovators', 'Ideation', 'Start', 'http://www.innovators.canterbury.ac.nz', 'UC InnovatorsÂ is a new initiative at the University of Canterbury dedicated to students to fuel innovation and the entrepreneurial spirit. UC Innovators is dedicated to nurturing student innovators and entrepreneurs. We help fast-track studentsâ€™ smart ideas to get them off the ground.', 1, 0, 0, 1, 0, 1, 1, 0, 0, 'UC Innov'),
(27, 'Christchurch Small Business Enterprise Centre (CSBEC)', 'Ideation', 'Grow', 'http://www.csbec.org.nz', 'CSBEC offers a range of services to start-up and existing micro and small business owners including business facilitation and consultancy, business development plans, marketing advice and reports, management planning and budgeting, finance advice, tourism consultancy and training courses. The Centre is an approved trainer, facilitator and vetting agency for the Enterprise Allowance Scheme provided by Work and Income.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'CSBEC'),
(44, 'MBIE', 'Plan', 'Start', 'http://www.business.govt.nz/starting-and-stopping/entering-a-business/starting-a-business', 'Guideline showing the Steps for starting a business (legal structure, taxes etc.)', 1, 1, 1, 1, 1, 1, 1, 1, 1, 'MBIE'),
(45, 'Lightning Lab', 'Ideation', 'Start', 'http://www.lightninglab.co.nz', 'Lightning Lab is an Accelerator programme that give startups the power to do more, faster. With seed capital, mentorship, extra hands on and the chance to focus on business 24/7 for 12 weeks. Acceleration provides a structure that brings focused support at the right time for the business. The core purpose of the Lightning Lab is to prepare companies for investors. The Lightning Lab is an investment pathway; seed funding and preparation for first external investment. It is also an internationally proven model for growing a strong entrepreneurial eco-system. It is a structure that enables investors and mentors to participate with and grow great companies but not carry the burden of the startup journey. It enables efficient support for seed stage startups.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'LL'),
(46, 'New Zealand Trade and Enterprise (NZTE)', 'Start', 'Grow', 'http://www.nzte.govt.nz', 'NZTE is Government''s international business development agency with a focus on New Zealand''s internationalising businesses. They work to increase New Zealand companies'' international success by helping them boost their global reach and build capability. They use their connections and government influence on behalf of businesses, and apply local knowledge from the NZTE team and a network of private sector experts to help them enter and grow in international markets.', 0, 0, 0, 0, 1, 1, 0, 0, 0, 'NZTE'),
(47, 'Ministry of Awesome', 'Ideation', 'Grow', 'http://www.ministryofawesome.com', 'Ministry of Awesome exists to water the seeds of awesome in Christchurch. We do this by bringing people together, providing social proof of others just like them doing awesome things, and offering structured support alogn the way. They connect ideas with the resources to turn them into reality -- people, funding, mentors, government entry points and enthusiasm, all with the aim of rippling awesome through our community to strengthen, encourage and inspire.', 1, 1, 1, 1, 1, 1, 1, 1, 1, 'MOA'),
(48, 'Canterbury Employers Chamber of Commerce', 'Ideation', 'Grow', 'http://cecc.org.nz', 'CECC exists to help local businesses to improve their enterprises, and to ensure our members can operate in a business friendly environment. Driven by approximately 2,550 members they are a notfor-profit organisation that have experienced business advisers offering advice to members, across all aspects of business including human resources, operations/health &amp; safety, marketing, business strategy, finance and sales. Our members enjoy free access to a host of useful resources, and receive significant discounts on all our events, networking evenings, training seminars and workshops.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'CECC'),
(49, 'Gap Filler', 'Ideation', 'Grow', 'http://www.gapfiller.org.nz', 'Gap Filler is a creative urban regeneration initiative that temporarily activates vacant sites within Christchurch with cool and creative projects, to make for a more interesting and vibrant city. They utilise vacant sites and spaces for temporary, creative, people centred purposes. They work with local community groups, artists, architects, landowners, librarians, designers, students, engineers, dancers anyone with an idea and initiative.', 1, 1, 1, 1, 1, 0, 0, 1, 0, 'GF'),
(50, 'Life in Vacant Spaces (LIVS)', 'Ideation', 'Start', 'http://livs.org.nz', 'LIVS acts as an umbrella organisation working on behalf of the transitional movement with Gap Filler, FESTA, Greening the Rubble, and many other groups and individuals. LIVS manages privately owned property for landowners and finds short and medium-term uses for the countless vacant sites and buildings of our city. Our mission is to cut through red tape and unlock permissions, making vacant space available to creative Christchurch and enabling hundreds of temporary activations.', 1, 1, 1, 1, 1, 1, 0, 1, 0, 'LIVS'),
(51, 'Exchange Christchurch (XCHC)', 'Start', 'Grow', 'http://www.xchc.co.nz', 'XCHC provides affordable production and showcase space to emerging businesses in the creative industries. XCHC was established as an experimental platform to bring together architecture, visual arts, performing arts, design, fashion, media and associated creative disciplines. They collaborate with local and international institutions such as galleries, museums, universities, cultural institutions as well as independent artists and performers.', 1, 1, 1, 1, 1, 1, 0, 1, 1, 'XCHC'),
(52, 'Powerhouse', 'Ideation', 'Grow', 'http://www.powerhouse-ventures.co.nz/commercialise', 'Powerhouse is an intellectual property commercialisation company using private and public investment to develop new spin-out ventures sourced primarily from research partners such as universities and Crown Research Institutes.', 1, 1, 1, 1, 1, 1, 0, 0, 0, 'PH'),
(53, 'Maori Business Facilitation Services', 'Start', 'Grow', 'http://www.tpk.govt.nz/whakamahia/maori-business-facilitation-service', 'The Maori Business Facilitation Service of Te Puni Kokiri can provide you with advice and guidance to new and existing Maori businesses.', 0, 0, 0, 1, 0, 1, 1, 0, 0, 'MBFS'),
(54, 'Christchurch City Council (CCC)', 'Start', 'Grow', 'http://ccc.govt.nz/index.aspx', 'The Christchurch City Council is committed to helping develop and grow your business, organisation and supporting your initiative becoming self-sufficient.', 1, 1, 1, 1, 1, 1, 0, 1, 1, 'CCC');

-- --------------------------------------------------------

--
-- Table structure for table `Services`
--

CREATE TABLE IF NOT EXISTS `Services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(35) NOT NULL,
  `moaID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `moaID` (`moaID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=508 ;

--
-- Dumping data for table `Services`
--

INSERT INTO `Services` (`id`, `service`, `moaID`) VALUES
(142, 'Mentoring', 22),
(143, 'Free one hour pre-mentoring service', 22),
(144, 'Business planning or business plan ', 22),
(277, 'Brokers temporary access', 50),
(278, 'Find temporary users', 50),
(279, 'Manages short term use', 50),
(280, 'Cover liabilities and insurance', 50),
(281, 'Basic maintainance', 50),
(282, 'Providing amenities', 50),
(336, 'Weekly and monthly forums', 47),
(337, 'Pitch coaching', 47),
(338, 'Start-Up activation', 47),
(339, 'Provide connections', 47),
(340, 'Co-working space', 47),
(341, 'Internships', 47),
(345, 'Facilitation', 53),
(346, 'Brokerage', 53),
(347, 'Mentoring', 53),
(348, 'Coaching', 53),
(349, 'Problem-solving', 53),
(350, 'Networking', 53),
(351, 'Accessing resources', 53),
(352, 'Referrals', 53),
(388, 'Funding', 23),
(436, 'Accelerator programme', 45),
(437, 'Seed funding', 45),
(438, 'Mentor-driven model', 45),
(439, 'Global accelerator network', 45),
(440, 'Workshops', 45),
(441, 'Workshops', 21),
(442, 'Clinics', 21),
(443, 'Launchpad', 21),
(444, 'Incubator', 21),
(445, 'Business development', 21),
(446, 'Investment strategy', 21),
(447, 'Advisory services', 21),
(448, 'Economic strategy', 24),
(449, 'Business development', 24),
(450, 'CDC innovation', 24),
(451, 'Competitions', 25),
(452, 'Educational workshops', 25),
(453, 'Networking workshops', 25),
(454, 'Hands on opportunities', 25),
(455, 'Industry exposure', 25),
(456, 'Biweekly expert advice sessions', 26),
(457, 'Student incubator -  "The Hatchery"', 26),
(458, 'Weekend start-up bootcamps', 26),
(459, 'Summer internships', 26),
(460, 'Summer start-up programmes', 26),
(461, 'Networking events', 26),
(462, 'Developing social enterprises', 26),
(463, 'Business facilitation', 27),
(464, 'Business consultancy', 27),
(465, 'Business plans', 27),
(466, 'Marketing advice', 27),
(467, 'Management planning', 27),
(468, 'Budgets', 27),
(469, 'Training courses', 27),
(470, 'Finance advice', 27),
(471, 'Business structure', 44),
(472, 'Business names', 44),
(473, 'Registration', 44),
(474, 'Business taxes', 44),
(475, 'Licences, consents and permits', 44),
(476, 'Further assistance', 44),
(477, 'Online export resources', 46),
(478, 'Market research database', 46),
(479, 'How-To-Guides for exporting', 46),
(480, 'Funding', 46),
(481, 'Building capability', 46),
(482, 'Streamline operations', 46),
(483, 'Marketing plans', 46),
(484, 'Develop strategies', 46),
(485, 'Membership', 48),
(486, 'Advice', 48),
(487, 'Consultancy', 48),
(488, 'Training seminars', 48),
(489, 'Workshops', 48),
(490, 'Resources', 48),
(491, 'Networking opportunities', 48),
(492, 'Project development', 49),
(493, 'Advice', 49),
(494, 'Volunteer programme', 49),
(495, 'Studio space', 51),
(496, 'Showcase space', 51),
(497, 'Co-working space', 51),
(498, 'Hosting events', 51),
(499, 'Commercialisation', 52),
(500, 'Seed funding', 52),
(501, 'Business incubation', 52),
(502, 'Venture capitalism', 52),
(503, 'Angel investment', 52),
(504, 'Provide information', 54),
(505, 'Funding', 54),
(506, 'Licenses and permits', 54),
(507, 'Resource consent', 54);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(128) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `salt`) VALUES
('awesome', '856a500a576ed6278a4a5356593cb2096802c3b622368eaf73adb65befd630b0c797415a35635863c9c56eb8cb6cf55454fb7703476e04bcb126c01eb6fc2fba', '7c0235c7634ce845c5c1d8fe5d427fb6927e9ed416ba3f58bfebde748f81448e7c70a72dcc562aa9c57ae68464795aef47956ac563eb2d9d72d85c12f28f1c3d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
