$(document).ready(function(){
	if($("form")[0]){
		$("form")[0].onsubmit = validateEdit;
	}
});

function validateEdit(e){
	if( validateOldUser() === false || validateNewUser() === false || validateOldPass() === false || validateNewPass() === false){
		e.preventDefault();
	}
}

function validateOldUser(){
	var user = document.forms["editForm"]["oldUsername"].value;
	if(user === null || user === ""){
		$(".oldUserError").html("Required");
		return false;
	}else if (user.match(/([^(\w+)*])+/g)) {
		$(".oldUserError").html("Username can only contain letters");
		return false;
	}else{
		$(".oldUserError").html("");
		return true;
	}
}

function validateNewUser(){
	var user = document.forms["editForm"]["newUsername"].value;
	if(user === null || user === ""){
		$(".newUserError").html("Required");
		return false;
	}else if (user.match(/([^(\w+)*])+/g)) {
		$(".newUserError").html("Username can only contain letters");
		return false;
	}else{
		$(".newUserError").html("");
		return true;
	}
}

function validateOldPass(){
	var pass = document.forms["editForm"]["oldPass"].value;
	if(pass === null || pass === ""){
		$(".oldPassError").html("Required");
		return false;
	}else if(pass.match(/([^(\w+)*])+/g)){
		$(".oldPassError").html("Password can only contain letters and Numbers");
		return false;
	}else{
		$(".oldPassError").html("");
		return true;
	} 
}

function validateNewPass(){
	var pass = document.forms["editForm"]["newPass"].value;
	if(pass === null || pass === ""){
		$(".newPassError").html("Required");
		return false;
	}else if(pass.match(/([^(\w+)*])+/g)){
		$(".newPassError").html("Password can only contain letters and Numbers");
		return false;
	}else{
		$(".newPassError").html("");
		return true;
	} 
}