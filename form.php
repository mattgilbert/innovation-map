<?php
session_start();
$servername = "localhost";
$username = "innomap";
$password = "YyUymm0TDF7saOir";
$dbname = "innomap";
?>
<!doctype html>

<html lang="en-GB">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="spiral.css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript" src="form.js"></script>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
<?php 
if(isset($_GET['id'])){
	echo "<title>Edit Organisation Form</title>";
}else{
	echo "<title>Add New Organisation Form</title>";
};
?>

<body>

<?php

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);


// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$id = "";$name = "";$start = "";$end = "";$website = "";$description = "";$student = "";$migrant = "";$creative = "";$maori = "";$other = "";$startupbusiness = "";$socialenterprise = "";$project = "";$event = "";$abbrv = "";$serviceList = array();
if(isset($_GET["id"])){
	
	$sql = "SELECT * FROM moa WHERE id = ?";
	$statement = $conn->prepare($sql);
	$statement->bind_param("i",$_GET["id"]);
	$statement->execute();
	$statement->bind_result($id,$name,$start,$end,$website,$description,$student,$migrant,$creative,$maori,$other,$startupbusiness,$socialenterprise,$project,$event,$abbrv);
	if(!$statement->fetch()){
		echo "no data found";
	}
	$statement->close();
	$sql2 = "SELECT service FROM Services WHERE moaID = ?";
	$statement = $conn->prepare($sql2);
	$statement->bind_param("i",$_GET["id"]);
	$statement->execute();
	$statement->bind_result($service);
	while($statement->fetch()){
		$serviceList[] = $service;
	}
}

if(isset($_SESSION['userLoggedIn'])){
	echo '<a class="btn listPage" href="list.php">List Page</a>';



	if(!isset($_GET["id"])){
		echo "<form name='org' action=".htmlspecialchars('addnew.php')." method='POST'>";
	}else{
		echo "<form name='org' action=".htmlspecialchars('edit.php')." method='POST'>";
	}
	?>
		<p class="title">Organisation:</p>
		<img src="assets/info_icon.png" class="nameInfo Info info" title="Required. Special characters not allowed."><label class="formLabel" for="name">Organisation Name:</label>
		<input class="text" type="text" name="name" onkeyup="validateName()" value="<?php echo $name; ?>" spellcheck="true"><span class="nameError error"></span>
		<br />
		<img src="assets/info_icon.png" class="abbrvInfo Info info" title="Required. Special characters not allowed. Max Length 15 characters."><label class="formLabel" for="abbrv">Name Abbreviation:</label>
		<input class="text" type="text" name="abbrv" onkeyup="validateAbbrv()" value="<?php echo $abbrv; ?>" spellcheck="false" maxlength = "15"><span class="abbrvError error"></span>
		<br />
		<img src="assets/info_icon.png" class="startInfo info" title="Required."><label class="formLabel" for="start">Spiral Start Position:</label>
		<select name="start" onchange="validateStart()">
			<option value=""> - - - - </option>
			<?php
			if($start == "Ideation"){
				echo "<option value='Ideation' selected>Ideation</option>";
			}else{
				echo "<option value='Ideation'>Ideation</option>";
			}
			if($start == "Plan"){
				echo "<option value='Plan' selected>Plan</option>";
			}else{
				echo "<option value='Plan'>Plan</option>";
			}
			if($start == "Start"){
				echo "<option value='Start' selected>Start</option>";
			}else{
				echo "<option value='Start'>Start</option>";
			}
			if($start == "Grow"){
				echo "<option value='Grow' selected>Grow</option>";
			}else{
				echo "<option value='Grow'>Grow</option>";
			}
			?>
		</select><span class="startError error"></span>
		<br />
		<img src="assets/info_icon.png" class="endInfo info" title="Required."><label class="formLabel" for="end">Spiral End Position:</label>
		<select name="end" onchange="validateEnd()">
			<option value=""> - - - - </option>
			<?php
			if($end == "Ideation"){
				echo "<option value='Ideation' selected>Ideation</option>";
			}else{
				echo "<option value='Ideation'>Ideation</option>";
			}
			if($end == "Plan"){
				echo "<option value='Plan' selected>Plan</option>";
			}else{
				echo "<option value='Plan'>Plan</option>";
			}
			if($end == "Start"){
				echo "<option value='Start' selected>Start</option>";
			}else{
				echo "<option value='Start'>Start</option>";
			}
			if($end == "Grow"){
				echo "<option value='Grow' selected>Grow</option>";
			}else{
				echo "<option value='Grow'>Grow</option>";
			}
			?>
		</select><span class="endError error"></span>
		<br />
		<img src="assets/info_icon.png" class="peopleInfo info" title="Required. More than one may be selected."><label class="formLabel" for="people">People:</label><span class="personError error"></span>
		<div class="checkboxPeople" onclick="validatePerson()">
			<?php
			if($student == 1){
				echo "<input type='checkbox' name='student' checked><span>Student</span><br />";
			}else{
				echo "<input type='checkbox' name='student'><span>Student</span><br />";
			}
			if($migrant == 1){
				echo "<input type='checkbox' name='migrant' checked><span>Migrant</span><br />";
			}else{
				echo "<input type='checkbox' name='migrant'><span>Migrant</span><br />";
			}
			if($creative == 1){
				echo "<input type='checkbox' name='creative' checked><span>Creative</span><br />";
			}else{
				echo "<input type='checkbox' name='creative'><span>Creative</span><br />";
			}
			if($maori == 1){
				echo "<input type='checkbox' name='maori' checked><span>Maori</span><br />";
			}else{
				echo "<input type='checkbox' name='maori'><span>Maori</span><br />";
			}
			if($other == 1){
				echo "<input type='checkbox' name='other' checked><span>Other</span><br />";
			}else{
				echo "<input type='checkbox' name='other'><span>Other</span><br />";
			}
			?>
		</div>
		<br />
		<img src="assets/info_icon.png" class="activitiesInfo info" title="Required. More than one may be selected."><label class="formLabel" for="activities">Activities:</label><span class="activityError error"></span>
		<div class="checkboxActivities" onclick="validateActivity()">
			<?php
			if($startupbusiness == 1){
				echo "<input type='checkbox' name='startUpBusiness' checked><span>Start-Up Business</span><br />";
			}else{
				echo "<input type='checkbox' name='startUpBusiness'><span>Start-Up Business</span><br />";
			}
			if($socialenterprise == 1){
				echo "<input type='checkbox' name='socialEnterprise' checked><span>Social Enterprise</span><br />";
			}else{
				echo "<input type='checkbox' name='socialEnterprise'><span>Social Enterprise</span><br />";
			}
			if($project == 1){
				echo "<input type='checkbox' name='project' checked><span>Project</span><br />";
			}else{
				echo "<input type='checkbox' name='project'><span>Project</span><br />";
			}
			if($event == 1){
				echo "<input type='checkbox' name='event' checked><span>Event</span><br />";
			}else{
				echo "<input type='checkbox' name='event'><span>Event</span><br />";
			}
			?>
		</div>
		<br />
		<img src="assets/info_icon.png" class="websiteInfo info" title="Required. Must be formatted correctly."><label class="formLabel" for="website">Website:</label>
		<input class="text" type="text" name="website" onkeyup="validateWebsite()" value="<?php echo $website; ?>" spellcheck="true"><span class="websiteError error"></span>
		<br />
		<img src="assets/info_icon.png" class="servicesInfo info" title="Required. Limit 10 and 35 characters each."><label class="formLabel" for="services">Services:</label>
		<div class="wrapper">
			<?php
			if(!isset($_GET["id"])){
				echo "<div class='serviceField'><span>1: </span><input type='text' name='service[]' class='service text servicesList' onkeyup='validateService(this)' maxlength='35' spellcheck='true'><span class='charNumService'></span><p class='serviceError1 error'></p></div>";
			}else{
				$x = 1;
				if(sizeof($serviceList) >= 1){
					foreach($serviceList as $service){
						if($x == 1){
							echo "<div class='serviceField'><span class='span'>".$x.":&nbsp</span><input type='text' name='service[]' class='service text servicesList' onkeyup='validateService(this)' value='$service' maxlength='35' spellcheck='true'><span class='charNumService'></span><p class='serviceError error'></p></div>";
						}else{
							echo "<div class='serviceField'><span class='span'>".$x.":&nbsp</span><input type='text' name='service[]' class='service text servicesList' onkeyup='validateService(this)' value='$service' maxlength='35' spellcSeheck='true'><a href='#' class='remove_field' title='Remove'>X</a><span class='charNumService'></span><p class='serviceError error'></p></div>";
						}
						$x++;
						?>
						<script>
							var x = <?php echo json_encode($x); ?>;
						</script>
						<?php
					}
				}else{
					echo "<div class='serviceField'><span>1: </span><input type='text' name='service[]' class='service text servicesList' onkeyup='validateService(this)' maxlength='35' spellcheck='true'><span class='charNumService1'></span><p class='serviceError1 error'></p></div>";
				}
			}
			?>
		</div>
		<br />
		<input type="button" class="addField" name="add" value="Add Service">
		<br />
		<br />
		<img src="assets/info_icon.png" class="descInfo info" title="Required. Limited to 250 characters."><label class="formLabel" for="description">Description:</label>
		<textarea class="description" cols="80" rows="7" onkeyup="validateDesc()" name="description" maxlength="800" spellcheck="true"><?php echo $description; ?></textarea><span class="charNumDesc"></span><span class="descError error"></span>
		<br />
		<?php
		if(!isset($_GET["id"])){
			echo "<input class='submitButton' type='submit' name='submit' value='Submit'>";
		}else{
			$input = $_GET["id"];
			echo '<input type="hidden" name="id" value="'.$input.'" />';
			echo "<input class='submitButton' type='submit' name='edit' value='Edit'>";
		}
	}else{
		header("Location: list.php");
		die();
	}
	?>
</form>

</body>

</html>