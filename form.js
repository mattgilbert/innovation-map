var x = 2; // initlal text box count
$(document).ready(function(){
	// text area character limit
	$('.description').keyup(function () {
	  // var max = 500;
	  var max = $(".description").attr("maxlength");
	  var len = $(this).val().length;
	  if (len >= max) {
	    $('.charNumDesc').text(' you have reached the limit');
	  } else {
	    var char = max - len;
	    $('.charNumDesc').text(char + ' characters left');
	  }
	});
	
	// add form field functionvar 
	var max_fields      = 10; // maximum input boxes allowed
    var wrapper         = $(".wrapper"); // Fields wrapper
    var add_button      = $(".addField"); // Add button ID
    
    $(add_button).click(function(e){ // on add input button click
        e.preventDefault();
        x = $(".serviceField").length + 1;
        if(x <= max_fields){ // max input box allowed
        	if(x < max_fields){
        		$(wrapper).append('<div class="serviceField"><span class="span">'+x+':&nbsp</span><input type="text" name="service[]" onkeyup="validateService(this)" class="service text servicesList" value=""/><a href="#" class="remove_field" title="Remove">X</a><span class="charNumService"></span><p class="serviceError error"></p></div>'); // add input box
        	}else{
            	$(wrapper).append('<div class="serviceField"><span class="span">'+x+':&nbsp</span><input type="text" name="service[]" onkeyup="validateService(this)" class="service10 text servicesList" value=""/><a href="#" class="remove_field" title="Remove">X</a><span class="charNumService"></span><p class="serviceError error"></p></div>'); // add input box
        	}
            if(x >= max_fields){
            	add_button.attr("disabled",true);
            }
            x++; // text box increment
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ // user click on remove text
        x=1;
        e.preventDefault();
        $(this).parent('div').remove();
        $(".serviceField").each(function(){
    		$(this).children(".span").html(x+":&nbsp");
    		x++;
        });

        add_button.attr("disabled",false)
        x--;
    });
    if($("form")[0]){
    	$("form")[0].onsubmit = validateAll;
    }
});

function validateName(){
	var name = document.forms["org"]["name"].value;
	if(name === null || name === ""){
		$(".nameError").html("Required");
		return false;
	}else if (name.match(/([^(\w+)*(\&*\(*\)*\-*\[*\]*\#*\@*\!*\?*\/*\"*\'*\:*\.*\é*)*(\s*)*])+/g)) {
		$(".nameError").html("Name can only contain letters and some special characters");
		return false;
	}else{
		$(".nameError").html("");
		return true;
	};
}

function validateAbbrv(){
	var abbrv = document.forms["org"]["abbrv"].value;
	if(abbrv === null || abbrv === ""){
		$(".abbrvError").html("Required");
		return false;
	}else if (name.match(/([^(\w+)*(\&*\(*\)*\-*\[*\]*\#*\@*\!*\?*\/*\"*\'*\:*\.*\é*)*(\s*)*])+/g)) {
		$(".abbrvError").html("Abbrveation can only contain letters and some special characters");
		return false;
	}else{
		$(".abbrvError").html("");
		return true;
	};
}

function validateStart(){
	var start = document.forms["org"]["start"].value
	if(start === ""){
		$(".startError").html("Required");
		return false;
	}else{
		$(".startError").html("");
		return true;
	};
}

function validateEnd(){
	var end = document.forms["org"]["end"].value;
	var endValue = document.forms["org"]["end"].selectedIndex;;
	if($("*[name='start']")[0].selectedIndex > endValue){
		$("*[name='start']")[0].selectedIndex = endValue;
	};
	if(end === ""){
		$(".endError").html("Required");
		return false;
	}else{
		$(".endError").html("");
		return true;
	};
}
	
function validateWebsite(){
	var website = document.forms["org"]["website"].value;
	var regex = /https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}/g;
	if(website === null || website === ""){
		$(".websiteError").html("Required");
		return false;
	}else if (regex.test(website) === true) {
		$(".websiteError").html("");
		return true;
	}else{
		$(".websiteError").html("Invalid website");
		return false;
	};
}

function validateDesc(){
	var txt = document.forms["org"]["description"].value;
	if(txt === null || txt === ""){
		$(".descError").html("Required");
		return false;
 	}else{
		$(".descError").html("");
		return true;
	};
}

function validatePerson(){
	var student = document.forms["org"]["student"].checked;
 	var migrant = document.forms["org"]["migrant"].checked;
 	var creative = document.forms["org"]["creative"].checked;
 	var maori = document.forms["org"]["maori"].checked;
 	var other = document.forms["org"]["other"].checked;
 	if(student === false && migrant === false && creative === false && maori === false && other === false){
 		$(".personError").html("Minimum 1 person required")
		return false;
 	}else{
		$(".personError").html("");
		return true;
	};
}

function validateActivity(){
	var startup = document.forms["org"]["startUpBusiness"].checked;
 	var social = document.forms["org"]["socialEnterprise"].checked;
 	var project = document.forms["org"]["project"].checked;
 	var even = document.forms["org"]["event"].checked;
 	if(startup === false && social === false && project === false && even === false){
 		$(".activityError").html("Minimum 1 activity required")
		return false;
 	}else{
		$(".activityError").html("");
		return true;
	};	
}

function validateService(services){
	var div = services.value;
	var max = $(".service").attr("maxlength");
	var len = div.length;
	var parent = $(services).parent();
	if (len >= max) {
		parent.children('.charNumService').text(' you have reached the limit');
	} else {
	var char = max - len;
		parent.children('.charNumService').text(char + ' characters left');
	};
	if(div.match(/([^a-zA-Z0-9\(\)\s\"\-\_\,])+/)){
		parent.children(".error").text("Invalid service");
	}else{
		parent.children(".error").text("");
		return true;
	}
}

function validateAll(e){
	if(validateName() === false || validateAbbrv() === false || validateStart() === false || validateStart() === false || validateEnd() === false || validatePerson() === false || validateActivity() === false || validateDesc() === false || validateWebsite() === false){
		e.preventDefault();
	}

};