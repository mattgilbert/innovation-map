<?php
$servername = "localhost";
$username = "innomap";
$password = "YyUymm0TDF7saOir";
$dbname = "innomap";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

if(isset($_POST["id"])){

	$student = 0;
	$migrant = 0;
	$creative = 0;
	$maori = 0;
	$other = 0;
	$sub = 0;
	$se = 0;
	$project = 0;
	$event = 0;

	$id = $_POST["id"];
	$name = $_POST['name'];
	$abbrv = $_POST['abbrv'];
	$start = $_POST['start'];
	$end = $_POST['end'];
	$website = rtrim($_POST['website'],"/");
	$desc = $_POST['description'];
	$student = isset($_POST['student']);
	$migrant = isset($_POST['migrant']);
	$creative = isset($_POST['creative']);
	$maori = isset($_POST['maori']);
	$other = isset($_POST['other']);
	$sub = isset($_POST['startUpBusiness']);
	$se = isset($_POST['socialEnterprise']);
	$project = isset($_POST['project']);
	$event = isset($_POST['event']);
	$serviceList = $_POST['service'];

	if (empty($_POST["name"])) {
	 $nameError = "Name is required";
	} else {
	 $name = test_input($_POST["name"]);
	}
	 
	if (empty($_POST["website"])) {
	 $website = "Website is required";
	} else {
	 $website = test_input($_POST["website"]);
	}

	if (empty($_POST["description"])) {
	 $descError = "Description is required";
	} else {
	 $desc = test_input($_POST["description"]);
	}


	$sql = "UPDATE `moa` SET `name`=?,`start`=?,`end`=?,`website`=?,`description`=?,`student`=?,`migrant`=?,`creative`=?,`maori`=?,`other`=?,`startupbusiness`=?,`socialenterprise`=?,`project`=?,`event`=?,`Abbrv`=? WHERE `id`=?";
	$statement = $conn->prepare($sql);
	$statement->bind_param("sssssiiiiiiiiisi",$name,$start,$end,$website,$desc,$student,$migrant,$creative,$maori,$other,$sub,$se,$project,$event,$abbrv,$id);
	$statement->execute();
	$statement->close();

	$sql = "SELECT id FROM moa WHERE name = ?";
	$statement = $conn->prepare($sql);
	$statement->bind_param("s",$name);
	$statement->execute();
	$statement->bind_result($id);
	if(!$statement->fetch()){
		"no data found";
	}
	$statement->close();

	$statement;
	$conn->autocommit(false);
	$sql = "DELETE FROM Services WHERE moaID = ?";
	if(!$statement = $conn->prepare($sql)){
		$conn->rollback();
		$conn->close();
		die('failed - 1');
	}
	$statement->bind_param("i",$id);
	if(!$statement->execute()){
		$conn->rollback();
		$conn->close();
		die('failed - 2');
	}
	$statement->close();

	$service;
	$sql = "INSERT INTO Services (service, moaID) value (?, ?)";
	if(!$statement = $conn->prepare($sql)){
		$conn->rollback();
		$conn->close();
		die('failed - 3');
	}
	$statement->bind_param("si", $service, $id);

	foreach($serviceList as $key=>$value){
		$service = $value;
		if(!$statement->execute()){
			$conn->rollback();
			$conn->close();
			die('failed - 4');
		}
	}
	$conn->commit();
	header('location: list.php');
}else{
	die('failed - 5');
}

?>